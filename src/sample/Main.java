package sample;

import DAO.ArticleDAO;
import DAO.StockDAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Article;
import model.Stock;
import view.ConnectionView;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main extends Application {

    public static EntityManagerFactory entityManagerFactory;
    @Override
    public void start(Stage primaryStage) throws Exception{
        entityManagerFactory = Persistence.createEntityManagerFactory("test");
        /*Article banane = new Article("banane","b0001",1.99F);
        ArticleDAO articleDAO = new ArticleDAO();
        banane = articleDAO.insert(banane);

        Stock stockBanane = new Stock();
        stockBanane.setId_article(banane.getId_article());
        stockBanane.setQuantite(20);
        StockDAO stockDAO = new StockDAO();
        stockDAO.insert(stockBanane);

        StockDAO stockDAO = new StockDAO();
        List<Stock> stock = stockDAO.getByIdArticle(1);

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Window");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        */

        ConnectionView connectionView = new ConnectionView();
        Stage connectionStage = new Stage();
        connectionView.start(connectionStage);
    }


    public static void main(String[] args) {
        Main.launch();
    }
}
