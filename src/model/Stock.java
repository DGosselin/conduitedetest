package model;

import javax.persistence.*;

@Entity
public class Stock {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_stock;
    private long id_article;
    private long id_rayon;
    private double quantite;

    public Stock() {
    }

    public Stock(long id_article, long id_rayon, double quantite) {
        this.id_article = id_article;
        this.id_rayon = id_rayon;
        this.quantite = quantite;
    }

    public long getId_stock() {
        return id_stock;
    }

    public void setId_stock(long id_stock) {
        this.id_stock = id_stock;
    }

    public long getId_article() {
        return id_article;
    }

    public void setId_article(long id_article) {
        this.id_article = id_article;
    }

    public long getId_rayon() {
        return id_rayon;
    }

    public void setId_rayon(long id_rayon) {
        this.id_rayon = id_rayon;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }
}
