package model;

import javax.persistence.*;

@Entity
public class Magasin {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_magasin;
    private String addresse;

    public Magasin() {
    }

    public Magasin(String addresse) {
        this.addresse = addresse;
    }

    public long getId_magasin() {
        return id_magasin;
    }

    public void setId_magasin(long id_magasin) {
        this.id_magasin = id_magasin;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }
}
