package model;

import javax.persistence.*;

@Entity
public class ChefMagasin {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_chefMagasin;
    private long id_utilisateur;
    private long id_magasin;

    public ChefMagasin() {
    }

    public ChefMagasin(long id_chefMagasin, long id_utilisateur, long id_magasin) {
        this.id_chefMagasin = id_chefMagasin;
        this.id_utilisateur = id_utilisateur;
        this.id_magasin = id_magasin;
    }

    public long getId_chefMagasin() {
        return id_chefMagasin;
    }

    public void setId_chefMagasin(long id_chefMagasin) {
        this.id_chefMagasin = id_chefMagasin;
    }

    public long getId_utilisateur() {
        return id_utilisateur;
    }

    public void setId_utilisateur(long id_uilisateur) {
        this.id_utilisateur = id_uilisateur;
    }

    public long getId_magasin() {
        return id_magasin;
    }

    public void setId_magasin(long id_magasin) {
        this.id_magasin = id_magasin;
    }
}
