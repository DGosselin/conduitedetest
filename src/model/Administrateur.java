package model;

import javax.persistence.*;

@Entity
public class Administrateur {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_administrateur;
    private long id_utilisateur;

    public Administrateur() {

    }

    public Administrateur(long id_administrateur, long id_utilisateur) {
        this.id_administrateur = id_administrateur;
        this.id_utilisateur = id_utilisateur;
    }

    public long getId_administrateur() {
        return id_administrateur;
    }

    public void setId_administrateur(long id_administrateur) {
        this.id_administrateur = id_administrateur;
    }

    public long getId_utilisateur() {
        return id_utilisateur;
    }

    public void setId_utilisateur(long id_utilisateur) {
        this.id_utilisateur = id_utilisateur;
    }
}
