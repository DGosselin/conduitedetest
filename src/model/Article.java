package model;

import javax.persistence.*;

@Entity
public class Article {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_article;
    private String nom;
    private String reference;
    private float prix;

    public Article() {
    }

    public Article(String nom, String reference, float prix) {
        this.nom = nom;
        this.reference = reference;
        this.prix = prix;
    }

    public long getId_article() {
        return id_article;
    }

    public void setId_article(long id_article) {
        this.id_article = id_article;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
