package model;

import javax.persistence.*;

@Entity
public class Rayon {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_rayon;
    private String nom;
    private long id_magasin;

    public Rayon() {
    }

    public Rayon(String nom, long id_magasin) {
        this.nom = nom;
        this.id_magasin = id_magasin;
    }

    public long getId_rayon() {
        return id_rayon;
    }

    public void setId_rayon(long id_rayon) {
        this.id_rayon = id_rayon;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getId_magasin() {
        return id_magasin;
    }

    public void setId_magasin(long id_magasin) {
        this.id_magasin = id_magasin;
    }
}
