package model;

import javax.persistence.*;

@Entity
public class ChefRayon {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_chefRayon;
    private long id_rayon;
    private long id_utilisateur;

    public ChefRayon() {
    }

    public ChefRayon(long id_chefRayon, long id_rayon, long id_utilisateur) {
        this.id_chefRayon = id_chefRayon;
        this.id_rayon = id_rayon;
        this.id_utilisateur = id_utilisateur;
    }

    public long getId_chefRayon() {
        return id_chefRayon;
    }

    public void setId_chefRayon(long id_chefRayon) {
        this.id_chefRayon = id_chefRayon;
    }

    public long getId_rayon() {
        return id_rayon;
    }

    public void setId_rayon(long id_rayon) {
        this.id_rayon = id_rayon;
    }

    public long getId_utilisateur() {
        return id_utilisateur;
    }

    public void setId_utilisateur(long id_utilisateur) {
        this.id_utilisateur = id_utilisateur;
    }
}
