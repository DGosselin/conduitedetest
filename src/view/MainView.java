package view;

import DAO.ArticleDAO;
import DAO.MagasinDAO;
import DAO.RayonDAO;
import DAO.StockDAO;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Article;
import model.Magasin;
import model.Rayon;
import model.Stock;

import java.util.*;


public class MainView extends Application {

    private ListView<String> storeList;
    private ListView<String> shelfList;
    private ListView<String> productList;
    private Button addButton;
    private Button removeButton;
    private Button modifButton;
    private TextField referenceField;
    private TextField nameField;
    private TextField quantityField;
    private TextField priceField;

    public ListView<String> getStoreList() {
        return storeList;
    }

    public void setStoreList(ListView<String> storeList) {
        this.storeList = storeList;
    }

    public ListView<String> getShelfList() {
        return shelfList;
    }

    public void setShelfList(ListView<String> shelfList) {
        this.shelfList = shelfList;
    }

    public ListView<String> getProductList() {
        return productList;
    }

    public void setProductList(ListView<String> productList) {
        this.productList = productList;
    }

    public Button getAddButton() {
        return addButton;
    }

    public void setAddButton(Button addButton) {
        this.addButton = addButton;
    }

    public Button getRemoveButton() {
        return removeButton;
    }

    public void setRemoveButton(Button removeButton) {
        this.removeButton = removeButton;
    }

    public Button getModifButton() {
        return modifButton;
    }

    public void setModifButton(Button modifButton) {
        this.modifButton = modifButton;
    }

    public TextField getReferenceField() {
        return referenceField;
    }

    public void setReferenceField(TextField referenceField) {
        this.referenceField = referenceField;
    }

    public TextField getNameField() {
        return nameField;
    }

    public void setNameField(TextField nameField) {
        this.nameField = nameField;
    }

    public TextField getQuantityField() {
        return quantityField;
    }

    public void setQuantityField(TextField quantityField) {
        this.quantityField = quantityField;
    }

    public TextField getPriceField() {
        return priceField;
    }

    public void setPriceField(TextField priceField) {
        this.priceField = priceField;
    }

    public MainView() {
    }



    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        primaryStage.setTitle("Gestionnaire de magasin");
        primaryStage.setScene(new Scene(root, 650, 650));
        primaryStage.show();

        //Initialisation des objets à afficher
        setStoreList(new ListView<String>());
        getStoreList().setMaxSize(150,250);

        setShelfList(new ListView<String>());
        getShelfList().setMaxSize(150,250);
        getShelfList().setDisable(true);

        setProductList(new ListView<String>());
        getProductList().setMaxSize(150,250);
        getProductList().setDisable(true);

        Label storeLabel = new Label("Magasins");
        Label shelfLabel = new Label("Rayons");
        Label productLabel = new Label("Produits");

        setReferenceField(new TextField());
        setQuantityField(new TextField());
        setNameField(new TextField());
        setPriceField(new TextField());
        getReferenceField().setDisable(true);
        getNameField().setDisable(true);
        getPriceField().setDisable(true);
        getQuantityField().setDisable(true);

        Label referenceLabel = new Label("Ref");
        Label nameLabel = new Label("Nom");
        Label quantityLabel = new Label("Qte");
        Label priceLabel = new Label("Prix");

        setAddButton(new Button("Ajouter"));
        setRemoveButton(new Button("Supprimer"));
        setModifButton(new Button("Modifier"));
        getAddButton().setDisable(true);
        getRemoveButton().setDisable(true);
        getModifButton().setDisable(true);

        //Placement des objets à afficher
        getStoreList().setLayoutX(50);
        getStoreList().setLayoutY(50);
        getShelfList().setLayoutX(250);
        getShelfList().setLayoutY(50);
        getProductList().setLayoutX(450);
        getProductList().setLayoutY(50);

        storeLabel.setLayoutX(50);
        storeLabel.setLayoutY(25);
        shelfLabel.setLayoutX(250);
        shelfLabel.setLayoutY(25);
        productLabel.setLayoutX(450);
        productLabel.setLayoutY(25);

        getReferenceField().setLayoutX(100);
        getReferenceField().setLayoutY(400);
        getNameField().setLayoutX(100);
        getNameField().setLayoutY(450);
        getQuantityField().setLayoutX(100);
        getQuantityField().setLayoutY(500);
        getPriceField().setLayoutX(100);
        getPriceField().setLayoutY(550);

        referenceLabel.setLayoutX(50);
        referenceLabel.setLayoutY(400);
        nameLabel.setLayoutX(50);
        nameLabel.setLayoutY(450);
        quantityLabel.setLayoutX(50);
        quantityLabel.setLayoutY(500);
        priceLabel.setLayoutX(50);
        priceLabel.setLayoutY(550);

        getAddButton().setLayoutX(300);
        getAddButton().setLayoutY(400);
        getModifButton().setLayoutX(300);
        getModifButton().setLayoutY(450);
        getRemoveButton().setLayoutX(300);
        getRemoveButton().setLayoutY(500);

        //Ajout des objets à afficher
        root.getChildren().add(getStoreList());
        root.getChildren().add(getShelfList());
        root.getChildren().add(getProductList());


        root.getChildren().add(storeLabel);
        root.getChildren().add(shelfLabel);
        root.getChildren().add(productLabel);


        root.getChildren().add(getReferenceField());
        root.getChildren().add(getNameField());
        root.getChildren().add(getPriceField());
        root.getChildren().add(getQuantityField());


        root.getChildren().add(referenceLabel);
        root.getChildren().add(nameLabel);
        root.getChildren().add(priceLabel);
        root.getChildren().add(quantityLabel);


        root.getChildren().add(getAddButton());
        root.getChildren().add(getRemoveButton());
        root.getChildren().add(getModifButton());

        MagasinDAO magasinDAO = new MagasinDAO();
        List<Magasin> magasinList = magasinDAO.getAll();
        ArrayList<String> magasinNameList = new ArrayList<String>();

        for (int i =0; i<magasinList.size(); i++) {
            long id = magasinList.get(i).getId_magasin();
            magasinNameList.add(String.valueOf(id));
        }

        ObservableList<String> magasinObservableList = FXCollections.observableList(magasinNameList);
        getStoreList().setItems(magasinObservableList);

        //Mise en place des listeners
        getStoreList().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue!=null) {
                    getShelfList().setDisable(false);

                    RayonDAO rayonDAO = new RayonDAO();
                    List<Rayon> rayonList= rayonDAO.getByIdRayon(Long.parseLong(newValue));
                    ArrayList<String> rayonNameList = new ArrayList<String>();

                    for (int i =0; i<rayonList.size(); i++) {
                        long id = rayonList.get(i).getId_rayon();
                        rayonNameList.add(String.valueOf(id));
                    }

                    ObservableList<String> rayonObservableList = FXCollections.observableList(rayonNameList);
                    getShelfList().setItems(rayonObservableList);
                }
            }
        });

        getShelfList().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue!=null) {
                    getProductList().setDisable(false);
                    getNameField().setDisable(false);
                    getPriceField().setDisable(false);
                    getQuantityField().setDisable(false);
                    getReferenceField().setDisable(false);

                    StockDAO stockDAO = new StockDAO();
                    List<Stock> stockList= stockDAO.getByIdRayon(Long.parseLong(newValue));
                    ArrayList<String> stockNameList = new ArrayList<String>();

                    for (int i =0; i<stockList.size(); i++) {
                        long id = stockList.get(i).getId_article();
                        stockNameList.add(String.valueOf(id));
                    }

                    ObservableList<String> rayonObservableList = FXCollections.observableList(stockNameList);
                    getProductList().setItems(rayonObservableList);
                }

                else{
                    getProductList().setDisable(true);
                    getNameField().setText("");
                    getReferenceField().setText("");
                    getPriceField().setText("");
                    getQuantityField().setText("");
                    getNameField().setDisable(true);
                    getReferenceField().setDisable(true);
                    getPriceField().setDisable(true);
                    getQuantityField().setDisable(true);
                }
            }
        });

        getProductList().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue!=null) {

                    ArticleDAO articleDAO = new ArticleDAO();
                    StockDAO stockDAO = new StockDAO();

                    Article article = articleDAO.getById(Long.parseLong(newValue));

                    getNameField().setText(article.getNom());
                    getPriceField().setText(String.valueOf(article.getPrix()));
                    getReferenceField().setText(article.getReference());
                    getQuantityField().setText(String.valueOf(stockDAO.getByIdArticle(Long.parseLong(newValue)).get(0).getQuantite()));

                    getAddButton().setDisable(true);
                    getModifButton().setDisable(false);
                    getRemoveButton().setDisable(false);
                }

                else{
                    getModifButton().setDisable(true);
                    getRemoveButton().setDisable(true);
                    getNameField().setText("");
                    getQuantityField().setText("");
                    getPriceField().setText("");
                    getReferenceField().setText("");
                }
            }
        });

        getPriceField().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(!getNameField().getText().isEmpty() && !getPriceField().getText().isEmpty() && !getQuantityField().getText().isEmpty() && !getReferenceField().getText().isEmpty() && getProductList().getSelectionModel().isEmpty()){
                    getAddButton().setDisable(false);
                }

                else{
                    getAddButton().setDisable(true);
                }
            }
        });

        getNameField().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(!getNameField().getText().isEmpty() && !getPriceField().getText().isEmpty() && !getQuantityField().getText().isEmpty() && !getReferenceField().getText().isEmpty() && getProductList().getSelectionModel().isEmpty()){
                    getAddButton().setDisable(false);
                }

                else{
                    getAddButton().setDisable(true);
                }
            }
        });

        getQuantityField().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(!getNameField().getText().isEmpty() && ! getPriceField().getText().isEmpty() && !getQuantityField().getText().isEmpty() && !getReferenceField().getText().isEmpty() && getProductList().getSelectionModel().isEmpty()){
                    getAddButton().setDisable(false);
                }

                else{
                    getAddButton().setDisable(true);
                }
            }
        });

        getReferenceField().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(!getNameField().getText().isEmpty() && ! getPriceField().getText().isEmpty() && !getQuantityField().getText().isEmpty() && !getReferenceField().getText().isEmpty() && getProductList().getSelectionModel().isEmpty()){
                    getAddButton().setDisable(false);
                }

                else{
                    getAddButton().setDisable(true);
                }
            }
        });

        getAddButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Article article = new Article(getNameField().getText(),getReferenceField().getText(),Float.valueOf(getPriceField().getText()));

                ArticleDAO articleDAO = new ArticleDAO();
                articleDAO.insert(article);

                Stock stock = new Stock(article.getId_article(),Integer.valueOf(getShelfList().getSelectionModel().getSelectedItem()),Double.valueOf(getQuantityField().getText()));

                StockDAO stockDAO = new StockDAO();
                stockDAO.insert(stock);

                List<Stock> stockList = stockDAO.getByIdRayon(Long.parseLong(getShelfList().getSelectionModel().getSelectedItem()));
                ArrayList<String> stockNameList = new ArrayList<String>();

                for (int i =0; i<stockList.size(); i++) {
                    long id = stockList.get(i).getId_article();
                    stockNameList.add(String.valueOf(id));
                }

                ObservableList<String> rayonObservableList = FXCollections.observableList(stockNameList);
                getProductList().setItems(rayonObservableList);


            }
        });

        getRemoveButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                Article article = new Article();
                article.setId_article(Long.valueOf(getProductList().getSelectionModel().getSelectedItem()));

                ArticleDAO articleDAO = new ArticleDAO();
                articleDAO.remove(article);

                StockDAO stockDAO = new StockDAO();

                List<Stock> stockList = stockDAO.getByIdRayon(Long.parseLong(getShelfList().getSelectionModel().getSelectedItem()));
                ArrayList<String> stockNameList = new ArrayList<String>();

                for (int i =0; i<stockList.size(); i++) {
                    long id = stockList.get(i).getId_article();
                    stockNameList.add(String.valueOf(id));
                }

                ObservableList<String> rayonObservableList = FXCollections.observableList(stockNameList);
                getProductList().setItems(rayonObservableList);

            }
        });

        getModifButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Article article = new Article(getNameField().getText(),getReferenceField().getText(),Float.valueOf(getPriceField().getText()));

                ArticleDAO articleDAO = new ArticleDAO();

                article.setId_article(Long.valueOf(getProductList().getSelectionModel().getSelectedItem()));

                articleDAO.modify(article);

                StockDAO stockDAO = new StockDAO();

                Stock stock = new Stock();
                List<Stock> stockList = stockDAO.getByIdArticle(article.getId_article());
                stock = stockList.get(0);
                stock.setQuantite(Double.valueOf(getQuantityField().getText()));

                stockDAO.modify(stock);

                stockList = stockDAO.getByIdRayon(Long.parseLong(getShelfList().getSelectionModel().getSelectedItem()));
                ArrayList<String> stockNameList = new ArrayList<String>();

                for (int i =0; i<stockList.size(); i++) {
                    long id = stockList.get(i).getId_article();
                    stockNameList.add(String.valueOf(id));
                }

                ObservableList<String> rayonObservableList = FXCollections.observableList(stockNameList);
                getProductList().setItems(rayonObservableList);

            }
        });
    }

}
