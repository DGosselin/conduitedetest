package view;

import DAO.UtilisateurDAO;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.stage.Stage;
import model.Utilisateur;

public class ConnectionView extends Application {

    public ConnectionView() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        primaryStage.setTitle("Gestionnaire de magasin");
        primaryStage.setScene(new Scene(root, 400, 175));
        primaryStage.show();

        //Initialisation des objets à afficher
        Button connectButton = new Button("Connecter");
        Button cancelButton = new Button("Annuler");
        TextField userField = new TextField();
        PasswordField pwField = new PasswordField();
        Label userLabel = new Label("Utilisateur");
        Label pwLabel = new Label("Mot de Passe");
        Label wrongPw = new Label("Mauvais mot de passe/utilisateur");
        wrongPw.setVisible(false);

        UtilisateurDAO utilisateurDAO = new UtilisateurDAO();

        connectButton.setOnAction(new EventHandler<ActionEvent>() {

            //!!!!!PLACEHOLDER AVANT CREATION DU CONTROLLEUR!!!!!
            public void handle(ActionEvent event) {
                if(!utilisateurDAO.getByLogin(userField.getText()).isEmpty()) {
                    if (utilisateurDAO.getByLogin(userField.getText()).get(0).getPassword().equals(pwField.getText())) {
                        MainView mainView = new MainView();
                        Stage mainStage = new Stage();

                        try {
                            mainView.start(mainStage);
                            primaryStage.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        wrongPw.setVisible(true);
                    }
                }
                else {
                    wrongPw.setVisible(true);
                }

            }

        });

        cancelButton.setOnAction(new EventHandler<ActionEvent>() {

            //!!!!!PLACEHOLDER AVANT CREATION DU CONTROLLEUR!!!!!
            public void handle(ActionEvent event) {
                primaryStage.close();
            }
        });

        //Placement des objets à afficher
        connectButton.setLayoutX(100);
        connectButton.setLayoutY(120);
        cancelButton.setLayoutX(250);
        cancelButton.setLayoutY(120);
        userField.setLayoutX(125);
        userField.setLayoutY(30);
        pwField.setLayoutX(125);
        pwField.setLayoutY(70);
        userLabel.setLayoutX(20);
        userLabel.setLayoutY(30);
        pwLabel.setLayoutX(20);
        pwLabel.setLayoutY(70);
        wrongPw.setLayoutX(100);
        wrongPw.setLayoutY(150);

        //Ajout des objets à afficher
        root.getChildren().add(connectButton);
        root.getChildren().add(cancelButton);
        root.getChildren().add(userField);
        root.getChildren().add(pwField);
        root.getChildren().add(userLabel);
        root.getChildren().add(pwLabel);
        root.getChildren().add(wrongPw);
    }

}
