package main;

import javafx.application.Application;
import javafx.stage.Stage;
import view.ConnectionView;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main extends Application {

    public static EntityManagerFactory entityManagerFactory;
    @Override
    public void start(Stage primaryStage) throws Exception{
        entityManagerFactory = Persistence.createEntityManagerFactory("test");

        ConnectionView connectionView = new ConnectionView();
        Stage connectionStage = new Stage();
        connectionView.start(connectionStage);
    }


    public static void main(String[] args) {
        Main.launch(args);
    }
}
