package DAO;

import model.Magasin;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class MagasinDAO {
    public List<Magasin> getAll(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Magasin> criteriaQuery = criteriaBuilder.createQuery(Magasin.class);
        Root<Magasin> root = criteriaQuery.from(Magasin.class);
        criteriaQuery.select(root);
        TypedQuery<Magasin> tq = entityManager.createQuery(criteriaQuery);

        List<Magasin>magasinList = tq.getResultList();
        entityManager.getTransaction().commit();

        return magasinList;
    }

    public Magasin getById(long id_magasin){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Magasin magasin = new Magasin();
        magasin.setId_magasin(id_magasin);

        magasin = entityManager.find(Magasin.class,magasin.getId_magasin());
        entityManager.getTransaction().commit();
        return magasin;
    }

    public Magasin insert(Magasin magasin) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(magasin);
        entityManager.getTransaction().commit();

        return magasin;
    }

    public void remove(Magasin magasin) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createQuery("delete from ChefMagasin where id_magasin = :id")
                .setParameter("id", magasin.getId_magasin())
                .executeUpdate();

        entityManager.remove(magasin);
        entityManager.getTransaction().commit();
    }
}
