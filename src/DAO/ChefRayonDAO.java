package DAO;

import model.ChefRayon;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class ChefRayonDAO {
    public List<ChefRayon> getByIdUtilisateur(long id_utilisateur){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<ChefRayon> criteriaQuery = criteriaBuilder.createQuery(ChefRayon.class);
        Root<ChefRayon> root = criteriaQuery.from(ChefRayon.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_utilisateur"), id_utilisateur));
        TypedQuery<ChefRayon> tq = entityManager.createQuery(criteriaQuery);
        List<ChefRayon> chefRayonList = tq.getResultList();
        entityManager.getTransaction().commit();

        return chefRayonList;
    }

    public ChefRayon insert(ChefRayon chefRayon) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(chefRayon);
        entityManager.getTransaction().commit();

        return chefRayon;
    }

    public void remove(ChefRayon chefRayon) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.remove(chefRayon);
        entityManager.getTransaction().commit();
    }
}
