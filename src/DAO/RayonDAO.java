package DAO;

import model.Rayon;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class RayonDAO {
    public Rayon getById(long id_rayon){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Rayon rayon = new Rayon();
        rayon.setId_rayon(id_rayon);

        rayon = entityManager.find(Rayon.class,rayon.getId_rayon());

        entityManager.getTransaction().commit();

        return rayon;
    }


    public List<Rayon> getByIdRayon(long id_magasin){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Rayon> criteriaQuery = criteriaBuilder.createQuery(Rayon.class);
        Root<Rayon> root = criteriaQuery.from(Rayon.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_magasin"), id_magasin));
        TypedQuery<Rayon> tq = entityManager.createQuery(criteriaQuery);

        List<Rayon> rayonList = tq.getResultList();
        entityManager.getTransaction().commit();
        return rayonList;
    }

    public Rayon insert(Rayon rayon) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(rayon);
        entityManager.getTransaction().commit();

        return rayon;
    }

    public void remove(Rayon rayon) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createQuery("delete from ChefRayon where id_rayon = :id")
                .setParameter("id", rayon.getId_rayon())
                .executeUpdate();

        entityManager.createQuery("delete from Stock where id_rayon = :id")
                .setParameter("id", rayon.getId_rayon())
                .executeUpdate();

        entityManager.remove(rayon);
        entityManager.getTransaction().commit();
    }
}
