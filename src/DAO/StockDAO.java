package DAO;

import model.Stock;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static main.Main.entityManagerFactory;

public class StockDAO {
    public List<Stock> getByIdArticle(long id_article){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Stock> criteriaQuery = criteriaBuilder.createQuery(Stock.class);
        Root<Stock> root = criteriaQuery.from(Stock.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_article"), id_article));
        TypedQuery<Stock> tq = entityManager.createQuery(criteriaQuery);
        List<Stock> stockList = tq.getResultList();
        entityManager.getTransaction().commit();
        return stockList;
    }

    public List<Stock> getByIdRayon(long id_rayon){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Stock> criteriaQuery = criteriaBuilder.createQuery(Stock.class);
        Root<Stock> root = criteriaQuery.from(Stock.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_rayon"), id_rayon));
        TypedQuery<Stock> tq = entityManager.createQuery(criteriaQuery);

        List<Stock> stockList = tq.getResultList();
        entityManager.getTransaction().commit();
        return stockList;
    }

    public Stock insert(Stock stock) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(stock);
        entityManager.getTransaction().commit();

        return stock;
    }

    public void remove(Stock stock) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.remove(entityManager.contains(stock) ? stock : entityManager.merge(stock));
        entityManager.getTransaction().commit();
    }

    public void modify(Stock stock){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(stock);
        entityManager.getTransaction().commit();
    }
}
