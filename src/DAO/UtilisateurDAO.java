package DAO;

import model.Utilisateur;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class UtilisateurDAO {
    public Utilisateur getById(long id_utilisateur){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setId_utilisateur(id_utilisateur);

        utilisateur = entityManager.find(Utilisateur.class,utilisateur.getId_utilisateur());
        entityManager.getTransaction().commit();
        return utilisateur;
    }

    public List<Utilisateur> getByLogin(String login){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Utilisateur> criteriaQuery = criteriaBuilder.createQuery(Utilisateur.class);
        Root<Utilisateur> root = criteriaQuery.from(Utilisateur.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("login"), login));
        TypedQuery<Utilisateur> tq = entityManager.createQuery(criteriaQuery);
        return tq.getResultList();
    }

    public List<Utilisateur> getByLoginPassword(String login, String password){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Utilisateur> criteriaQuery = criteriaBuilder.createQuery(Utilisateur.class);
        Root<Utilisateur> root = criteriaQuery.from(Utilisateur.class);
        criteriaQuery.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get("login"), login),
                        criteriaBuilder.equal(root.get("password"), password)
                )
        );
        TypedQuery<Utilisateur> tq = entityManager.createQuery(criteriaQuery);
        List<Utilisateur> utilisateurList = tq.getResultList();
        entityManager.getTransaction().commit();
        return utilisateurList;
    }

    public Utilisateur insert(Utilisateur utilisateur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(utilisateur);
        entityManager.getTransaction().commit();

        return utilisateur;
    }

    public void remove(Utilisateur utilisateur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createQuery("delete from ChefRayon where id_utilisateur = :id")
                .setParameter("id", utilisateur.getId_utilisateur())
                .executeUpdate();

        entityManager.createQuery("delete from ChefMagasin where id_utilisateur = :id")
                .setParameter("id", utilisateur.getId_utilisateur())
                .executeUpdate();

        entityManager.createQuery("delete from Administrateur where id_utilisateur = :id")
                .setParameter("id", utilisateur.getId_utilisateur())
                .executeUpdate();

        entityManager.remove(utilisateur);
        entityManager.getTransaction().commit();
    }
}
