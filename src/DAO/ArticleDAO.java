package DAO;

import model.Article;

import javax.persistence.EntityManager;

import static main.Main.entityManagerFactory;

public class ArticleDAO {
    public Article getById(long id_article){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Article article = new Article();
        article.setId_article(id_article);

        article = entityManager.find(Article.class,article.getId_article());
        entityManager.getTransaction().commit();
        return article;
    }

    public Article insert(Article article){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(article);
        entityManager.getTransaction().commit();

        return article;
    }

    public void remove(Article article) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createQuery("delete from Stock where id_article = :id")
                .setParameter("id", article.getId_article())
                .executeUpdate();

        entityManager.remove(entityManager.contains(article) ? article : entityManager.merge(article));
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void modify(Article article) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(article);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
