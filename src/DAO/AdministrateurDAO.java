package DAO;

import model.Administrateur;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class AdministrateurDAO {
    public List<Administrateur> getByIdUtilisateur(long id_utilisateur){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Administrateur> criteriaQuery = criteriaBuilder.createQuery(Administrateur.class);
        Root<Administrateur> root = criteriaQuery.from(Administrateur.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_utilisateur"), id_utilisateur));
        TypedQuery<Administrateur> tq = entityManager.createQuery(criteriaQuery);
        List<Administrateur> administrateurList = tq.getResultList();
        entityManager.getTransaction().commit();
        return administrateurList;
    }

    public Administrateur insert(Administrateur administrateur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(administrateur);
        entityManager.getTransaction().commit();

        return administrateur;
    }

    public void remove(Administrateur administrateur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.remove(administrateur);
        entityManager.getTransaction().commit();
    }
}
