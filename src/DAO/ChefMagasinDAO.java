package DAO;

import model.ChefMagasin;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static main.Main.entityManagerFactory;

public class ChefMagasinDAO {
    public List<ChefMagasin> getByIdUtilisateur(long id_utilisateur){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<ChefMagasin> criteriaQuery = criteriaBuilder.createQuery(ChefMagasin.class);
        Root<ChefMagasin> root = criteriaQuery.from(ChefMagasin.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id_utilisateur"), id_utilisateur));
        TypedQuery<ChefMagasin> tq = entityManager.createQuery(criteriaQuery);
        List<ChefMagasin> chefMagasinList = tq.getResultList();
        entityManager.getTransaction().commit();
        return chefMagasinList;
    }

    public ChefMagasin insert(ChefMagasin chefMagasin) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(chefMagasin);
        entityManager.getTransaction().commit();

        return chefMagasin;
    }

    public void remove(ChefMagasin chefMagasin) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.remove(chefMagasin);
        entityManager.getTransaction().commit();
    }
}
